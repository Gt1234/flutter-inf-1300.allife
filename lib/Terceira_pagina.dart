import 'package:flutter/material.dart';
import 'Segunda_pagina.dart';

class Terceirapag extends StatelessWidget{
  get backgroundColor => Colors.white;
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      theme: ThemeData(primaryColor: Colors.deepOrange),
      home: Mostra_tarefas(),
    );
  }
}

class Mostra_tarefas extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return Mostra_tarefasState();
  }
}

class Mostra_tarefasState extends State<Mostra_tarefas > {
  String tarefa = 'Comer';
  String hour = '12:50';
  @override

  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
            centerTitle: true, backgroundColor: Colors.deepPurpleAccent,
            title: Text('Tarefas',
                style: TextStyle(fontStyle: FontStyle.italic,fontSize: 20,
                    decorationColor: Colors.black))
        ),
        body: ListView(
            children:<Widget>[
              ListTile(
                leading: Icon(Icons.assignment),
                title: Text('${tarefa}'),
                subtitle: Text('${hour}'),
                onTap: () {showDialog(context: context,
                    barrierDismissible: false,
                    builder: (BuildContext context){
                      return AlertDialog(
                          title: Text('Editar',style: TextStyle(fontStyle: FontStyle.italic),),
                          content: ListTile(title: TextField(
                            onChanged: (texto_novo){tarefa = texto_novo;},
                            decoration: InputDecoration(labelText: 'Tarefa',labelStyle: TextStyle(fontStyle: FontStyle.italic)),)),
                          actions: <Widget>[
                            RaisedButton(child: Text('Confirmar'),onPressed:(){
                              setState(() {});
                              Navigator.of(context).pop();
                            })
                          ]
                      );
                    });
                },
              )
            ]
        )
    );
  }
}




