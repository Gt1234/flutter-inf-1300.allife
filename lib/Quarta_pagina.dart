import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'Segunda_pagina.dart';

class Quarta_page extends StatelessWidget{
  get backgroundColor => Colors.white;
  @override
  Widget build(BuildContext context){
    return MaterialApp(
      theme: ThemeData(brightness: Brightness.dark,primaryColor: Colors.deepOrange,accentColor: Colors.blue),
      home: Quartapag(),
    );
  }
}


class Quartapag extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return QuartapagState();
  }
}

class QuartapagState extends State<Quartapag > {
  String nome = '';
  String hobby = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(centerTitle: true,backgroundColor: Colors.deepPurpleAccent,shadowColor: Colors.deepOrangeAccent,
        title: Text('Perfil',style: TextStyle(fontStyle: FontStyle.italic,fontSize: 20)),),
      body: GestureDetector(onTap:(){;},
        child: ListView(shrinkWrap: true,children: <Widget>[ListTile(title: TextField(onChanged:
            (name_user){nome=name_user;},decoration:InputDecoration(
            labelText: 'Nome',labelStyle: TextStyle(color: Colors.black,fontSize: 20,fontStyle: FontStyle.italic)))),
          ListTile(title: TextField(onChanged:(hobby_user){
            hobby=hobby_user;},
              decoration: InputDecoration(labelText: 'Hobby',labelStyle: TextStyle(
              color: Colors.black,fontSize: 20, fontStyle: FontStyle.italic)))),
          RaisedButton(padding: const EdgeInsets.all(1),
            child: Text('Atualizar',style: TextStyle(fontStyle: FontStyle.italic)),onPressed: (){
            Navigator.popUntil(context, ModalRoute.withName('/first'));},)
        ]),
      ),
    );
  }
}