import 'package:flutter/material.dart';
import 'dart:io';
import 'Pagina_inicial.dart';
import 'main.dart';



class Secondpage extends StatefulWidget {
  @override
  _SecondpageState createState() => _SecondpageState();
}

class _SecondpageState extends State<Secondpage> {
  String tarefa='';
  String horario='';
  String horario2 = '';
  List<String> tar_hour=[];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: SingleChildScrollView(
        child: SizedBox(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Padding(
            padding: const EdgeInsets.all(25.0),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Image.network('https://s3.amazonaws.com/ibc-portal/wp-content/uploads/2017/03/20152508/1-gestao-de-tarefas.jpg'),
                TextField(
                    onChanged: (text){
                      tarefa = text;
                    },
                    decoration: InputDecoration(
                        labelText: 'Tarefa',
                        border: OutlineInputBorder()
                    ),
                    ),
                TextField(
                    onChanged: (text2){
                      horario = text2;
                    },
                    decoration: InputDecoration(
                        labelText: 'Horário',
                        border: OutlineInputBorder()
                    )
                ),
                RaisedButton(onPressed: () {
                  try {horario2 = horario.substring(0, 2) + '.' + horario.substring(3,);
                       double horario3 = double.parse(horario2);
                       int horario4 = int.parse(horario.substring(0 ,2));
                       int horario5 = int.parse(horario.substring(3,));
                       if(horario.length == 5 && horario4 >=0 && horario4<24 && horario5>=0 && horario5<60 && horario.substring(2,3)==':' && horario != ''){
                          tar_hour.add(tarefa);
                          tar_hour.add(horario);
                          print(tar_hour);
                          Navigator.popUntil(context, ModalRoute.withName('/first'));}
                       else{showDialog(context: context,
                           barrierDismissible: false,
                           builder: (BuildContext context){
                             return AlertDialog(
                               title: Text('Digite um horario valido!'),
                               actions:<Widget> [
                                 RaisedButton(child: Text('Ok'),
                                     onPressed: (){Navigator.of(context).pop();})
                               ],
                             );
                           });}}
                  on Exception catch (_) {
                    showDialog(context: context,
                        barrierDismissible: false,
                        builder: (BuildContext context){
                      return AlertDialog(
                        title: Text('Digite um horario valido!'),
                        actions:<Widget> [
                          RaisedButton(child: Text('Ok'),
                              onPressed: (){Navigator.of(context).pop();})
                        ],
                      );
                        });}
                  },
                  child:Text('Adicionar'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}



